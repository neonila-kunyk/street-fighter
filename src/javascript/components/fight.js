import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  const leftFighter = initFighter(firstFighter, 'left');
  const rightFighter = initFighter(secondFighter, 'right');
  
  document.addEventListener('keydown', (event) => { keyDownHandler(event, leftFighter, rightFighter) }, false);
  document.addEventListener('keyup', (event) => { keyUpHandler(event, leftFighter, rightFighter) }, false);

  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over
    const timer = setInterval(()=> {
      if (leftFighter.currentHealth === 0 || rightFighter.currentHealth === 0) {
        document.removeEventListener('keydown', (event) => { keyDownHandler(event, leftFighter, rightFighter) }, false);
        document.removeEventListener('keyup', (event) => { keyUpHandler(event, leftFighter, rightFighter) }, false);
        clearInterval(timer);
        const winner = leftFighter.currentHealth === 0 ? rightFighter : leftFighter;
        resolve(winner);
      }
    }, 100)
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = getRandom(1, 2);
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = getRandom(1, 2);
  return fighter.defense * dodgeChance;
}

function getRandom(min, max) {
  return Math.random() * (max - min) + min;
}

function keyDownHandler(event, leftFighter, rightFighter) {
  switch(event.code){
    case controls.PlayerOneAttack: {
      attack(leftFighter, rightFighter);
      break;
    }
    case controls.PlayerOneBlock: {
      blockOn(leftFighter);
      break;
    }
    case controls.PlayerOneCriticalHitCombination[0]: {
      criticalHitKeyDownHandler (event.code, leftFighter, rightFighter);
      break;
    }
    case controls.PlayerOneCriticalHitCombination[1]: {
      criticalHitKeyDownHandler (event.code, leftFighter, rightFighter);
      break;
    }
    case controls.PlayerOneCriticalHitCombination[2]: {
      criticalHitKeyDownHandler (event.code, leftFighter, rightFighter);
      break;
    }
    case controls.PlayerTwoAttack: {
      attack(rightFighter, leftFighter);
      break;
    }
    case controls.PlayerTwoBlock: {
      blockOn(rightFighter);
      break;
    }
    case controls.PlayerTwoCriticalHitCombination[0]: {
      criticalHitKeyDownHandler (event.code, rightFighter, leftFighter);
      break;
    }
    case controls.PlayerTwoCriticalHitCombination[1]: {
      criticalHitKeyDownHandler (event.code, rightFighter, leftFighter);
      break;
    }
    case controls.PlayerTwoCriticalHitCombination[2]: {
      criticalHitKeyDownHandler (event.code, rightFighter, leftFighter);
      break;
    }
  }
}

function keyUpHandler(event, leftFighter, rightFighter) {
  switch(event.code){
    case controls.PlayerOneBlock: {
      blockOff(leftFighter);
      break;
    }
    case controls.PlayerOneCriticalHitCombination[0]: {
      criticalHitKeyUpHandler (event.code, leftFighter);
      break;
    }
    case controls.PlayerOneCriticalHitCombination[1]: {
      criticalHitKeyUpHandler (event.code, leftFighter);
      break;
    }
    case controls.PlayerOneCriticalHitCombination[2]: {
      criticalHitKeyUpHandler (event.code, leftFighter);
      break;
    }
    case controls.PlayerTwoBlock: {
      blockOff(rightFighter);
      break;
    }
    case controls.PlayerTwoCriticalHitCombination[0]: {
      criticalHitKeyUpHandler (event.code, rightFighter);
      break;
    }
    case controls.PlayerTwoCriticalHitCombination[1]: {
      criticalHitKeyUpHandler (event.code, rightFighter);
      break;
    }
    case controls.PlayerTwoCriticalHitCombination[2]: {
      criticalHitKeyUpHandler (event.code, rightFighter);
      break;
    }
  }
}

function initFighter(givenFighter, position) {
  const fighter = {...givenFighter};
  fighter.block = false;
  fighter.currentHealth = givenFighter.health;
  fighter.canCriticalHit = true;
  fighter.position = position;
  if (position === 'left'){
    for (let key of controls.PlayerOneCriticalHitCombination) {
      fighter[key] = false;
    }
  }
  else if (position === 'right') {
    for (let key of controls.PlayerTwoCriticalHitCombination){
      fighter[key] = false;
    }
  }
  return fighter;
}

function attack(attacker, defender) {
  const damage = attacker.block || defender.block ? 0 : getDamage(attacker, defender);
  defender.currentHealth -= damage;
  decreaseHealthBar(defender);
}

function blockOn(fighter) {
  fighter.block = true;
}

function blockOff(fighter) {
  fighter.block = false;
}


function decreaseHealthBar(fighter){
  if(fighter.currentHealth < 0) fighter.currentHealth = 0;
    const change = fighter.currentHealth * 100 / fighter.health;
    document.querySelector(`#${fighter.position}-fighter-indicator`).style.width = `${change}%`;
}

function criticalHitKeyDownHandler (key, attacker, defender) {
  if (!attacker.canCriticalHit || attacker.block) return;
  attacker[key] = true;
  let isCriticalHit = true;
  const combination = attacker.position === 'left' ? controls.PlayerOneCriticalHitCombination : controls.PlayerTwoCriticalHitCombination;
  Object.keys(attacker).forEach((key) => {
    if(combination.includes(key) && attacker[key] === false) {
      isCriticalHit = false;
    }
  })
  if (isCriticalHit){
    attacker.canCriticalHit = false;
    attacker.needChange = true;
    defender.currentHealth -= attacker.attack * 2;
    decreaseHealthBar(defender);
    setTimeout(()=>{
      attacker.canCriticalHit = true;
    }, 10000)
  }
}

function criticalHitKeyUpHandler(key, fighter) {
  fighter[key] = false;
}