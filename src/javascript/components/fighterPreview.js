import { createElement } from '../helpers/domHelper';
import { fighters } from '../helpers/mockData';
//import { fighters } from '../helpers/mockData';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });

  // todo: show fighter info (image, name, health, etc.)
  const infoElement = createElement({
    tagName: 'div',
    className: `fighter-preview___info`,
  });
  if(!fighter) return fighterElement;
  Object.keys(fighter)
    .filter(key => key !== '_id' && key !== 'source')
    .forEach((key)=>{
      const element = createElement({
        tagName: 'div',
        className: `fighter-preview___prop`,
      });
      const title = createElement({
        tagName: 'div',
        className: `title`,
      });
      const value = createElement({
        tagName: 'div',
        className: `value`,
      });
      title.innerText = `${key}: `;
      value.innerText = String(fighter[key]);
      element.append(title, value);
      infoElement.append(element);
    })
  const imageElement = createFighterImage(fighter);
  fighterElement.append(infoElement, imageElement);
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
