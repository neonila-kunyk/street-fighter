import { fighters } from "../../helpers/mockData"
import {showModal} from "./modal"
import { createElement } from '../../helpers/domHelper';
import { createFighters } from '../../components/fightersView';

export function showWinnerModal(fighter) {
  // call showModal function 
  const bodyElement = createElement({ tagName: 'div', className: 'modal-body' });
  const winnerNameElement = createElement({ tagName: 'span', className: 'modal___winner-name' });
  winnerNameElement.innerText = fighter.name;
  bodyElement.append(winnerNameElement);

  const modal = {
    title: "And the winner becomes...",
    bodyElement,
    onClose: showStartStage
  }
  showModal(modal);
}

function showStartStage () {
  const root = document.getElementById('root');
  root.innerHTML = '';
  const fightersElement = createFighters(fighters);
  root.appendChild(fightersElement);
}
